# Distance-based Mapping in R

Distance-based mapping using R, coordinated by Make.

To make sure everything works (you'll need to have `ggplot2`,`dplyr`,`purrr` and `readr` installed), run `make map` from within the `hotspotr` directory. If everything works, there should be an example map at `output/hotspot_map.jpg` that looks like the one on this page.

To start making maps, you'll need a CSV file with three columns: `x` & `y`, giving the coordinates of your data points and `case`, containing binary indicators for case vs. control status (case = 1, control = 0).

Then create a YAML file with the following fields:

- **`mapdata`** is the path to the file containing your input CSV.
- **`outdir`** is the path to the directory you want to write everything out to.
- **`cellwidth`** is the width of the raster cells in the resulting output (these are assumed to be square, so width = height). Be careful to pick a value that is on the right scale of your data, i.e. if the data are over many kilometers, cells of 0.01 KM x 0.01 KM will result in a very large raster and the program  will run *very* slowly. Similarly, if your data are on the unit square, cells of 0.1 x 0.1 will result in a very coarse map.
- **`p`** is the proportion of the total population of cases and controls used to calculate smoothing windows around each grid cell. 
- **`circlen`** is the number of external points to use to create the map (10 is usually good).
- **`mapname`** is the name of the map you want to generate (ggplot will create the right type out of output file based on the file extension).

For a template, see the `test_parameters.yml` file within the `hotspotr` folder.

Finally, when you're all set to create some maps, run `./make_map <yourparameters.yml>` from the command line, where `<yourparameters.yml>` is the name of your input parameters file.
