OUTDIR := output
DATADIR := $(OUTDIR)/data
MAPDATA := $(DATADIR)/maptest.csv
METHOD := dbm
CELLWIDTH := 0.01
P := 0.1
CIRCLEN := 10
PARS := test_parameters.yml
MAPNAME := hotspot_map.jpg

.PHONY: map
map: $(OUTDIR)/$(MAPNAME)

$(DATADIR)/maptest.csv : src/test_data.R
	@echo --- Generating test data ---
	@echo $(DATADIR)
	@mkdir -p $(@D)
	./$<

$(DATADIR)/circle_coords.csv : src/circle_points.R $(MAPDATA) $(PARS)
	@echo --- Generating enclosing circle ---
	@mkdir -p $(@D) 
	./$< -o $@ -d $(word 2, $^) -n $(CIRCLEN)

$(DATADIR)/data_grid.csv : src/make_grid.R $(MAPDATA) $(PARS)
	@echo --- Making grid ---
	@mkdir -p $(@D) 
	./$< -o $@ -d $(word 2, $^) -w $(CELLWIDTH)

## Method for making the dbm lookup
$(DATADIR)/dbm_grid_lookup.Rds : src/grid_point_distance.R $(MAPDATA) $(DATADIR)/circle_coords.csv $(DATADIR)/data_grid.csv $(PARS)
	@echo --- Calculating DBM grid point neighborhoods ---
	@mkdir -p $(@D) 
	./$<  -o $@ -d $(word 2, $^) -c $(word 3, $^) -r $(word 4, $^) -p $(P)

## Method for making the knn lookup
$(DATADIR)/knn_grid_lookup.Rds : src/find_nearest_neighbors.R $(MAPDATA) $(DATADIR)/data_grid.csv $(PARS)
	@echo --- Calculating KNN grid point neighborhoods ---
	@mkdir -p $(@D) 
	./$< -o $@ -d $(word 2, $^) -r $(word 3, $^) 


$(DATADIR)/scored_grid.csv : src/calculate_score.R $(MAPDATA) $(DATADIR)/$(METHOD)_grid_lookup.Rds  $(DATADIR)/data_grid.csv lib/map_score.R
	@echo --- Calculating score for data ---
	@mkdir -p $(@D)
	./$< -o $@ -d $(word 2, $^) -n $(word 3, $^) -r $(word 4, $^)

$(DATADIR)/random_scores.csv : src/random_scores.R $(MAPDATA) $(DATADIR)/$(METHOD)_grid_lookup.Rds  $(DATADIR)/data_grid.csv lib/map_score.R
	@echo --- Calculating random scores ---
	@mkdir -p $(@D)
	./$< -o $@ -d $(word 2, $^) -n $(word 3, $^) -r $(word 4, $^)


$(OUTDIR)/$(MAPNAME) : src/colorscale.R $(DATADIR)/random_scores.csv $(DATADIR)/scored_grid.csv
	@echo --- Making map ---
	@mkdir -p $(@D)
	./$< -o $@ -r $(word 2, $^) -d $(word 3, $^)

web/images/hotspot_map.jpg : $(OUTDIR)/hotspot_map.jpg
	@mkdir -p $(@D)
	cp $< $@

.PHONY: deploy
deploy: web/images/hotspot_map.jpg

.PHONY: clean
clean:
	rm -r $(OUTDIR)
