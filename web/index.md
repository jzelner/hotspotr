---
layout: default
---
<a href="https://gitlab.com/jzelner/hotspotr/commits/master"><img alt="build status" src="https://gitlab.com/jzelner/hotspotr/badges/master/build.svg"/></a>

[Hotspotr](https://gitlab.com/jzelner/hotspotr) is an example of distance-based mapping in R


![Hotspot Map](images/hotspot_map.jpg)

To get started with building your own maps, just clone the repository with: `git clone https://gitlab.com/jzelner/hotspotr`.

To make sure everything works, first run `./install.R` to make sure all of the required packages are installed.
Then run `make map` from within the `hotspotr` directory. If everything works, there should be an example map at `output/hotspot_map.jpg` that looks like the one on this page. If you want to see how `hotspotr` performs on different kinds of maps, change the parameters in `src/test_data.R` and re-run `make map`.

To start making maps with your own data, you'll need a CSV file with three columns: `x` & `y`, giving the coordinates of your data points and `case`, containing binary indicators for case vs. control status (case = 1, control = 0).

Then create a YAML file like this one either within or outside of the `hotspotr` folder:

{% highlight yaml %}
mapdata: output/data/maptest.csv
outdir: output
cellwidth: 0.01
p: 0.1
circlen: 10
mapname: hotspot_map.jpg
{% endhighlight %}
- **`mapdata`** is the path to the file containing your input CSV.
- **`outdir`** is the path to the directory you want to write everything out to.
- **`cellwidth`** is the width of the raster cells in the resulting output (these are assumed to be square, so width = height). Be careful to pick a value that is on the right scale of your data, i.e. if the data are over many kilometers, cells of 0.01 KM x 0.01 KM will result in a very large raster and the program  will run *very* slowly. Similarly, if your data are on the unit square, cells of 0.1 x 0.1 will result in a very coarse map.
- **`p`** is the proportion of the total population of cases and controls used to calculate smoothing windows around each grid cell. 
- **`circlen`** is the number of external points to use to create the map (10 is usually good).
- **`mapname`** is the name of the map you want to generate (ggplot will create the right type out of output file based on the file extension).

For a template, see the `test_parameters.yml` file within the `hotspotr` folder.

Finally, when you're all set to create some maps, run `./make_map <yourparameters.yml>` from the command line, where `<yourparameters.yml>` is the name of your input parameters file.
