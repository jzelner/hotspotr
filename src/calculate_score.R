#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   calculate_score.R [-d <data> -o <outdir> -r <grid> -n <grid input>]

Options:
   -d CSV file with samples [default: output/data/maptest.csv]
   -n Grid input [default: output/data/knn_grid_lookup.Rds]
   -r Data grid [default: output/data/data_grid.csv]
   -o Grid point data [default: output/data/scored_grid.csv]
 ' -> doc

opts <- docopt(doc)

suppressMessages(require(dplyr))
suppressMessages(require(readr))
suppressMessages(require(ggplot2))
suppressMessages(require(purrr))

source("lib/map_score.R")

grid_d <- read_csv(opts$r)
point_d <- read_csv(opts$d)
neighbors <- readRDS(opts$n)

grid_d$density <- calculate_score(neighbors, point_d)

write_csv(grid_d, opts$o)
