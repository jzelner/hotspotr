#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   colorscale.R [-r <random> -o <outfile> -d <grid>]

Options:
   -r File random scores for colorscale [default: output/data/random_scores.csv]
   -d Scored grid [default: output/data/scored_grid.csv]
   -o Output image [default: output/hotspot_map.jpg]
 ' -> doc

opts <- docopt(doc)
suppressMessages(require(readr))
suppressMessages(require(dplyr))
suppressMessages(require(ggplot2))

## Load randomized scores
random_scores <- read_csv(opts$r)

## Get upper and lower quantiles
upper_q <- quantile(random_scores$high_scores, probs = c(0.95))[[1]]
lower_q <- quantile(random_scores$low_scores, probs = c(0.05))[[1]]

## Load grid
grid_d <- read_csv(opts$d)

grid_d %>%
    mutate(hotspot = ifelse(density >= upper_q, 1, 0),
           coldspot = ifelse(density <= lower_q, 1, 0)) -> grid_d


g <- ggplot() +
    geom_raster(data = filter(grid_d, hotspot == 0, coldspot == 0),aes(x=x,y=y,fill = density)) +
    geom_raster(data = filter(grid_d, hotspot == 1), aes(x=x,y=y), fill = "red") +
    geom_raster(data = filter(grid_d, coldspot == 1), aes(x=x,y=y), fill = "blue") +
    scale_fill_continuous(low = "green", high = "orange") + coord_equal()

ggsave(opts$o)
