#!/usr/bin/Rscript
suppressMessages(require(readr))
suppressMessages(require(dplyr))
suppressMessages(require(ggplot2))

N <- 5000
xlim <- c(0, 1)
ylim <- c(0, 1)

hotspot_xlim <- c(0.45, 0.65)
hotspot_ylim <- c(0.45, 0.65)

in_square <- function(x, y, xmin, xmax, ymin, ymax) {
    inside_count <- (x >= xmin) + (x <= xmax) + (y >= ymin) + (y <= ymax)
    return(inside_count == 4)
}

xcoord <- xlim[1] + runif(N)*(xlim[2]-xlim[1])
ycoord <- ylim[1] + runif(N)*(ylim[2]-ylim[1])

base_case_prob <- 0.01
hotspot_or <- 25.0

hotspot_case_prob <- plogis(log(hotspot_or) + qlogis(base_case_prob))
df <- data.frame(x = xcoord,
                 y = ycoord)

df %>%
    mutate(hotspot = in_square(x,y, hotspot_xlim[1], hotspot_xlim[2], hotspot_ylim[1], hotspot_ylim[2]),
           case_prob= ifelse(hotspot == TRUE, hotspot_case_prob, base_case_prob)) %>%
    mutate(case = rbinom(nrow(df),1,case_prob)) -> out_data

out_data$point_id <- 1:nrow(out_data)

write_csv(out_data, "output/data/maptest.csv")
