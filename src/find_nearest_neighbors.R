#!/usr/bin/Rscript
suppressMessages(require(docopt))
'Usage:
   find_nearest_neighbors.R [-d <data> -o <outdir> -r <grid> -p <window>]

Options:
   -d CSV file with samples [default: output/data/maptest.csv]
   -o Output data [default: output/data/nearest_neighbors.Rds]
   -r Grid point data [default: output/data/data_grid.csv]
   -p Proportion of controls to use as neighbors [default: 0.1]
 ' -> doc

opts <- docopt(doc)

suppressMessages(require(readr))
suppressMessages(require(dplyr))

## Load point data
read_csv(opts$d) %>%
    select(point_id, case, x, y) -> point_d

## Calculate number of neighbors
k <- ceiling(as.numeric(opts$p)*nrow(point_d))

## Load grid data
grid_d <- read_csv(opts$r)


## Loop through individual points
out_d <- matrix(0, nrow(grid_d),k)
index <- 1
for (i in 1:nrow(grid_d)) {
    gx <- grid_d$x[i]
    gy <- grid_d$y[i]

    point_d$gd <- sqrt((point_d$x-gx)**2 + (point_d$y-gy)**2)

    neighbors <- point_d$point_id[order(point_d$gd)[1:k]]
    out_d[i,] <- neighbors

}

saveRDS(out_d,opts$o)
